# = Class: solr::core
#
# This class downloads and installs a Apache Solr
#
# == Parameters:
#
# $solr_version:: which version of solr to install
#
# $solr_home:: where to place solr
#
#
# == Requires:
#
#   wget
#
# == Sample Usage:
#
#   class {'solr::core':
#     solr_version           => '4.4.0'
#   }
#
class solr::core(
  $solr_version = $solr::params::solr_version,
  $solr_home = $solr::params::solr_home,
  $apache_mirror = $solr::params::apache_mirror,
  $core_name = $solr::params::core_name,
  $init_solrxml = $solr::params::init_solrxml

) inherits solr::params {

  # using the 'creates' option here against the finished product so we only download this once

  $solr_tgz_url = "https://s3-us-west-2.amazonaws.com/metabiota-public/solr/solr-${solr_version}.tgz"
  $solt_jars_zip_url = "https://s3-us-west-2.amazonaws.com/metabiota-public/solr/solr_jars.zip"
  user { "solr":
    ensure => present,
    home   => '/var/lib/solr',
    uid    => '4000'
  } ->

  exec { "wget solr":
    command => "wget --output-document=/tmp/solr-${solr_version}.tgz ${solr_tgz_url}",
    creates => "${solr_home}/solr-${solr_version}",
    user    => solr
  } ->

  file { "/var/lib/solr":
    ensure => directory,
    owner  => solr
  } ->

  exec { "untar solr":
    command => "tar -xf /tmp/solr-${solr_version}.tgz -C ${solr_home}",
    creates => "${solr_home}/solr-${solr_version}",
    user    => solr
  } ->

  file { "${solr_home}/current":
    ensure => link,
    target => "${solr_home}/solr-${solr_version}",
    owner  => solr,
  }

  # defaults if solr_conf is not provided
  # data will go to /var/lib/solr
  # conf will go to /etc/solr
  file { "/etc/solr":
    ensure => directory,
    owner  => solr,
  } ->

  file { "/etc/solr/collection1":
    ensure => directory,
    owner  => solr,
  } ->

  file { "/etc/solr/collection1/conf":
    ensure => directory,
    owner  => solr,
  } ->

  file { "/var/lib/solr/lib":
    ensure => directory,
    owner  => solr
  }

  if $init_solrxml {
    file { "/var/lib/solr/solr.xml":
    ensure => present,
    source => "puppet:///modules/solr/solr.xml",
    owner  => solr,
    require => File["/etc/solr"]
  }
}
  exec { "wget solr_jars":
    command => "wget --output-document=/var/lib/solr/lib/solr_jars.zip $solt_jars_zip_url",
    creates => "/var/lib/solr/lib/solr_jars.zip",
    require => File["/var/lib/solr/lib"]
  }

  exec { "unzip solr_jars":
    command => "unzip /var/lib/solr/lib/solr_jars.zip -d /var/lib/solr/lib/",
    user => solr,
    require => Exec["wget solr_jars"],
    creates => "/var/lib/solr/lib/mysql-connector-java-5.1.39-bin.jar"
  }
}