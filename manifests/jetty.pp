# = Class: solr::jetty
#
# This class manages the default jetty installation included with Apache Solr
#
# == Parameters:
#
#
# == Requires:
#
#
# == Sample Usage:
#
#
class solr::jetty(
  $solr_version = $solr::params::solr_version,
  $solr_home = $solr::params::solr_home,
  $zookeeper_hosts = $solr::params::zookeeper_hosts,
  $core_name = $solr::params::core_name,
  $hdfs_home = "localhost",
  $solr_ha = $solr::params::solr_ha,
  $hdfs_nameservices = "",
  $hdfs_namenode1_name = "",
  $hdfs_namenode2_name = "",
  $hdfs_namenode1_host = "",
  $hdfs_namenode2_host = "",
  $solr_ha_options = '-Dsolr.directoryFactory=HdfsDirectoryFactory" "-Dsolr.hdfs.confdir=/etc/hadoop'

) inherits solr::params {

  class { 'solr::core':
	    core_name => $core_name }

  if $operatingsystem == "Ubuntu" {
      exec { "load init.d into upstart":
        command => "update-rc.d solr defaults",
        user    => "root",
        onlyif  => "test 7 != `ls -al /etc/rc*.d | grep solr | wc | awk '{print \$1}'`" ,
        require => [File["/etc/init.d/solr"], Class['solr::core']]
        # checks if solr service is enabled
      }  
  }

  file { "/etc/init.d/solr":
    ensure => "present",
    mode   => '0755',
    content => template("solr/solr.erb"),
    owner  => 'root',
    notify => Service["solr"]
  } ->

  file { "/etc/default/solr-jetty":
    content => template("solr/solr-jetty.erb"),
    ensure => present,
    owner  => 'root',
    notify => Service["solr"]
  } ->

  service {"solr":
    ensure  => running,
    require => Class["solr::core"]
  }
  # HA support for HDFS endpoint

  if $solr_ha {
    file { "/etc/hadoop":
      ensure => directory,
      owner  => solr
    }
    file { "/etc/hadoop/core-site.xml":
      ensure => present,
      content => template("solr/hdfs/core-site.xml.erb"),
      owner  => solr,
      require => File["/etc/hadoop"]
    }
    file { "/etc/hadoop/hdfs-site.xml":
      ensure => present,
      content => template("solr/hdfs/hdfs-site.xml.erb"),
      owner  => solr,
      require => File["/etc/hadoop"]
    }
  }
}
