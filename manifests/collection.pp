define solr::collection(
  $db_endpoint = 'localhost',
  $db_user = 'dbroot',
  $db_passwd = 'password',
  $init_collection = 'false',
  $solr_host = 'localhost',
  $zk_host = 'localhost',
  $zk_cli_script_path = '/var/lib/solr/current/example/scripts/cloud-scripts/',
  $replication_factor = 1,
  $max_shards_per_node = 1,
  $num_shards = 1
  ){
  file { "/var/lib/solr/current/example/solr/${name}":
    ensure => directory,
    owner  => solr
  } ->
  file { "/var/lib/solr/current/example/solr/${name}/config":
    ensure => directory,
    owner  => solr
  } ->
  file { "/var/lib/solr/current/example/solr/${name}/config/data-config.xml":
    content => template("solr/geocode/${name}/data-config.erb"),
    ensure => present,
    owner  => 'solr',
  } ->
  file { "/var/lib/solr/current/example/solr/${name}/config/schema.xml":
    content => template("solr/geocode/${name}/schema.erb"),
    ensure => present,
    owner  => 'solr',
  } ->
  file { "/var/lib/solr/current/example/solr/${name}/config/solrconfig.xml":
    content => template("solr/geocode/${name}/solrconfig.erb"),
    ensure => present,
    owner  => 'solr',
  }
  if ( $init_collection == true ){
    exec {"sleep-60-${name}":
      command => "sleep 60",
      timeout => 400,
      path =>"/usr/bin:/usr/sbin:/bin:/sbin",
      require => [File["/var/lib/solr/current/example/solr/${name}/config/data-config.xml"], File["/var/lib/solr/current/example/solr/${name}/config/schema.xml"], File["/var/lib/solr/current/example/solr/${name}/config/solrconfig.xml"]]
    }
    exec { "Upload ${name} collection configs to Zookeeper":
      command => "/bin/sh  ${zk_cli_script_path}zkcli.sh -cmd upconfig -zkhost ${zk_host}:2181 -confname ${name} -confdir /var/lib/solr/current/example/solr/${name}/config",
      path    => ['/bin/bash','/usr/bin','/bin/sh'],
      cwd     => $zk_cli_script_path,
      user    => solr,
      require => [Exec["sleep-60-${name}"], File["/var/lib/solr/current/example/solr/${name}/config/data-config.xml"], File["/var/lib/solr/current/example/solr/${name}/config/schema.xml"], File["/var/lib/solr/current/example/solr/${name}/config/solrconfig.xml"]]
    } ->
    exec { "Create ${name} collection":
      command => "wget \"http://${solr_host}:8983/solr/admin/collections?action=CREATE&name=${name}&numShards=${num_shards}&replicationFactor=${replication_factor}&maxShardsPerNode=${max_shards_per_node}&collection.configName=${name}\"  -O /tmp/solr_${name}_create_collection",
      path    => '/usr/bin',
      user    => solr,
      creates => '/tmp/solr_${name}_create_collection',
      require => [Exec["Upload ${name} collection configs to Zookeeper"]]
    } ->
    exec { "Full-index for ${name} collection":
      command => "wget \"http://${solr_host}:8983/solr/${name}/dataimport?command=full-import\" -O /tmp/solr_${name}_index_collection",
      path    => '/usr/bin',
      user    => solr,
      creates => '/tmp/solr_${name}_index_collection',
      require => [Exec["Create ${name} collection"]]
    }
  }
}